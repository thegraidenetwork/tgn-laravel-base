# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

### Added
- Nothing yet.

## [7.0.0] - 2020-06-18

### Removed
- Got rid of `RolePermissions` middleware as we've moved to role-based authentication in our application.

### Changed
- Upgraded to Laravel 6.0
- Upgraded to PHP Unit 9.0
- Upgraded other dev packages

## [6.0.0] - 2019-06-21

### Changed
- Upgraded to Laravel 5.8
- Upgraded to PHP Unit 8.0
- Upgraded slevomat/coding-standard

## [5.0.1] - 2019-06-21

### Fixed
- Bad reference to Archivable Scope in Archivable Trait.

## [5.0.0] - 2019-06-21

### Changed
- Upgraded to Laravel 5.7
- Updated minimum PHP version 7.3

### Removed
- Base Archive class and Archive/Recover Jobs.

## [4.1.2] - 2018-11-05

### Fixed
- Fixed namespace infidelity.

## [4.1.1] - 2018-11-02

### Fixed
- Fixed linting errors caused by missing `strict_types` declarations.

## [4.1.0] - 2018-11-01

### Added
- `Archivable` trait and `ArchivableScope` global scope.
- `Archive` and `Recover` jobs.
- `BaseArchive` abstract class for API-specific `archive` Command implementations.
- `GraideNetwork\Base\Models\Helpers` for common Model-related helper functions.

## [4.0.0] - 2018-09-12

### Changed
- Upgraded to Laravel 5.6
- Updated minimum PHP version 7.2
- Repository update and delete methods use `findOrFail` to throw 404 exceptions instead of fatal errors

### Added
- `UpdatesEmbeddedObjects`, `DeletesEmbeddedObjects`, `CreatesEmbeddedObjects` traits.

## [3.1.2] - 2018-04-30

### Fixed
- Fixing bug in abstract model requiring array for scope filters when it needs to allow null.

## [3.1.1] - 2018-04-11

### Fixed
- Path to `HttpException` class explicitly set to `Symfony\Component\HttpKernel\Exception\HttpException`.

## [3.1.0] - 2018-03-23

### Added
- Role Based Permission middleware and configuration file.
- Strict type declarations and linting rule to enforce these declarations.

### Changed
- Refactoring internal controller methods to return `422` status code when validation fails instead of `400` error.

## [3.0.0-rc1] - 2018-02-22

### Fixed
- Code coverage working in container now.
- Updating fields from assignment api update.
- Exception type hinting in AbstractController.
- Edge case where a resource doesn't exist and no exception was being thrown (now throws not found exception).

### Removed
- Dependency on tgn-clients, Guzzle as they should be sibling dependencies.

### Changed
- Upgraded to Laravel 5.5.
- Repository Update/Delete methods find a model instance then act on it allowing us to overload the `update` and `delete` methods on a model and get expected behavior.

### Added
- More precise type hinting in base jobs, DTOs, repositories and middleware.
- Codesniffer linter.

## [2.0.0] - 2018-1-21

Version 2.0 was an attempt to remove the Repositories from the package, and diverged from the 1.x branch. Version 3.0 took some elements from v2.0 and some from v1.x, so the history of v2.0 is kept in a separate branch.

## [1.1.0] - 2017-7-19

### Added
- HomeController as base for API landing pages.

## [1.0.2] - 2017-7-6

### Fixed
- More errors for strict return types so I've made most unspecified

## [1.0.1] - 2017-7-3

### Fixed
- Return types for update, patch, delete were set to bool instead of int

## [1.0.0] - 2017-6-29

### Added
- Strong typing to abstract controller.
- Docker support for running tests, continuous integration.

### Changed
- Require Laravel >5.4, PHP >7.1
- Merged Dispatch controller trait into abstract controller to make it the default.

### Fixed
- User `qouta` fields in tests.

## [0.7.7] - 2017-1-12

### Changed
- `tgn-clients` dependency version to `dev-master` to get upstream changes quicker.

## [0.7.6] - 2016-12-21

### Changed
- Abstract model filter scope.

## [0.7.5] - 2016-11-18

### Added
- Slack notifications route.

## [0.7.4] - 2016-11-14

### Added
- Updated `tgn-clients` to add support for students endpoint in courses API.

## [0.7.3] - 2016-11-09

### Added
- Updated `tgn-clients` to add support for rubrics API.

## [0.7.2] - 2016-11-07
### Changed
- Fixed bug in integrators by updating `tgn-clients` package.

## [0.7.1] - 2016-11-03
### Changed
- Removed Clients from this package, replaced with `thegraidenetwork/tgn-clients` package.

## [0.7.0] - 2016-10-31
### Added
- User and Base DTOs as stand-in for Eloquent models in supporting APIs.

## [0.6.0] - 2016-10-26
### Added
- New API endpoints to Users, Assignments clients.

## [0.5.2] - 2016-10-07
### Fixed
- Abstract repository wasn't paginating correctly if pages not used in query string.

## [0.5.1] - 2016-09-29
### Added
- Unit tests for the abstract model to ensure query builder methods are being called.

## [0.5.0] - 2016-09-21
### Added
- Dispatch controller trait. Overwrites resource methods to use Jobs rather than direct to repositories.
- Base job classes for CRUD operations.
- Base TestCase method for unit tests with method to access protected properties.

### Fixed
- Some docblocks that were inaccurate.

### Changed
- Internal `resourceGetOne` method to `resourceGetById` for consistency with repository and job classes.
- Moved all tests to proper folders for consistent namespacing with `src/` directory.
- All tests now extend a base TestCase to allow for shared helper methods.

## [0.4.3] - 2016-09-20
### Added
- Status code 204 response when POSTing to a resource that does not necessitate response data.

## [0.4.2] - 2016-09-16
### Fixed
- Failure handling for delete method in controller.
- Matching `deleteById` signature in controller with that found in repository.

## [0.4.1] - 2016-09-14
### Added
- Controller methods to allow overwriting for dispatching jobs or saving to repository (default).
- Abstract controller can dispatch jobs by default.

## [0.4.0] - 2016-09-14
### Added
- Added Authenticate middleware to handle basic auth.
- Added internal abstract API client and tests.
- Added Assignments API client.
- Added Courses API client.
- Added Users API client.

## [0.3.0] - 2016-09-09
### Added
- Allowing Laravel 5.3, php7 only.
- Adding unit tests, code coverage.
- Adding concrete fixtures to assist in testing abstract classes.

### Fixed
- Made AbstractModel _actually_ abstract.

### Removed
- Unused `ValidatesRequest` class from abstract controller.

## [0.2.3] - 2016-09-09
### Fixed
- Added BaseRepositoryInterface.

## [0.2.2] - 2016-09-07
### Fixed
- Added Validator facade to abstract controller to fix bug.

## [0.2.0] - 2016-09-01
### Added
- Initial release:
  - DevEnv middleware class to lock down endpoints from production.
  - Documentation for using and updating this package.

## [0.1.0] - 2016-08-29
### Added
- Initial release:
  - Documentation
  - Abstract controller and repository
  - Model scopes in use in Assignments API

### Changed
- N/A

### Removed
- N/A
