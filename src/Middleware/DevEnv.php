<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DevEnv
{
    /**
     * Makes sure that the current environment is not "production" before allowing request.
     *
     * @param Request $request
     * @param Closure $next
     * @param null $guard
     *
     * @return Closure | Response
     */
    public function handle(Request $request, Closure $next, $guard = null)
    {
        if (env('APP_ENV') != 'production') {
            return $next($request);
        }
        return new Response('Invalid environmental variable.', 403);
    }
}
