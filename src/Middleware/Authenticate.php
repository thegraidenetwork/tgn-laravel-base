<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class Authenticate
{
    /**
     * Checks to make sure basic auth headers are added to the request
     *
     * @param Request $request
     * @param Closure $next
     * @param $guard
     *
     * @return Closure | Response
     */
    public function handle(Request $request, Closure $next, $guard = null)
    {
        if (env('APP_ENV') === 'testing') {
            // Skips basic auth for testing
            return $next($request);
        }
        // Validate username and password
        if ($this->onceBasic($request->getUser(), $request->getPassword())) {
            return $next($request);
        }
        // If auth fails, send back a 401
        $headers = ['WWW-Authenticate' => 'Basic'];
        return new Response('Invalid credentials.', 401, $headers);
    }

    private function onceBasic($username, $password = null): bool
    {
        $correctUsername = env('BASIC_AUTH_USERNAME');
        $correctPassword = env('BASIC_AUTH_PASSWORD');
        if ($correctUsername && $correctPassword) {
            if ($correctUsername === $username && $correctPassword === $password) {
                return true;
            }
        }
        return false;
    }
}
