<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Dtos;

class BaseDto
{
    public function __construct($attributes = [])
    {
        foreach ($attributes as $key => $attribute) {
            if (property_exists($this, $key)) {
                $this->$key = $attribute;
            }
        }
    }

    public static function hydrate($attributes = []): BaseDto
    {
        $object = get_called_class();
        return new $object($attributes);
    }
}
