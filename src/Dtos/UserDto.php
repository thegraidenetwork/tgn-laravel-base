<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Dtos;

use Illuminate\Notifications\Notifiable;

class UserDto extends BaseDto
{
    use Notifiable;

    public $ID;

    public $email;

    public $first_name;

    public $last_name;

    public $phone_number;

    public function routeNotificationForSlack(): string
    {
        return env('SLACK_ACTIVITY_WEBHOOK_URL');
    }
}
