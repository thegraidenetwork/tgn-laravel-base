<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Repositories\Contracts;

use GraideNetwork\Base\Models\AbstractModel;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface BaseRepositoryInterface
{
    // Read
    public function get(array $options = []): LengthAwarePaginator;

    // Read by id
    public function getById(int $id, array $options = []): AbstractModel;

    // Create
    public function create(array $data = []): AbstractModel;

    // Edit
    public function updateById(int $id, array $data = []): bool;

    // Delete
    public function deleteById(int $id): bool;
}
