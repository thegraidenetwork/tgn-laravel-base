<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Repositories;

use GraideNetwork\Base\Models\AbstractModel;
use GraideNetwork\Base\Repositories\Contracts\BaseRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Str;

abstract class AbstractRepository implements BaseRepositoryInterface
{
    /**
     * Fields that can be used for ordering
     *
     * @var array
     */
    protected $allowedOrderBy = [];

    /**
     * Models to attach by default to this item
     *
     * @var array
     */
    protected $defaultModels = [];

    /**
     * Field to use for ordering
     *
     * @var string
     */
    protected $defaultOrderBy = 'id';

    /**
     * Order of results
     *
     * @var 'asc' | 'desc'
     */
    protected $defaultOrder = 'asc';

    /**
     * Model for this repository
     *
     * @var AbstractModel
     */
    protected $model;

    /**
     * Get a paginated collection of models with an options array
     *
     * @param array $options Options array
     *
     * @return LengthAwarePaginator
     */
    public function get(array $options = []): LengthAwarePaginator
    {
        // Set up the query
        $query = $this->instantiateQuery();

        // Update query for scope fields
        $query = $this->callModelScopeMethods($query, $options);

        // Attach any models that are associated
        $query = $this->attachModels($query, $options);

        // Return paginated results
        return $this->executePaginatedQuery($query, $options);
    }

    /**
     * Get a single Model based on the value of a field (returns the first result)
     *
     * @param string $field Field to query on
     * @param string $value Value to search for
     * @param array $options Options array
     *
     * @return AbstractModel
     */
    public function getBy(string $field, $value = null, array $options = []): AbstractModel
    {
        // Filter by the requested field
        $query = $this->model->where($field, $value);

        // Attach any models that are associated
        $query = $this->attachModels($query, $options);

        return $this->executeQuery($query, $options);
    }

    /**
     * Get a single result based on the value of a the id field
     *
     * @param int $id id to search for
     * @param array $options Options array
     *
     * @return AbstractModel
     */
    public function getById(int $id, array $options = []): AbstractModel
    {
        return $this->getBy('id', $id, $options);
    }

    /**
     * Create a single item using a data array
     *
     * @param array $data Data array
     *
     * @return AbstractModel
     */
    public function create(array $data = []): AbstractModel
    {
        return $this->model->create($data);
    }

    /**
     * Find and update a resource by its id
     *
     * @param int $id id to update
     * @param array $data Data array
     *
     * @return boolean
     */
    public function updateById(int $id, array $data = []): bool
    {
        return $this->model
            ->findOrFail($id)
            ->update($data);
    }

    /**
     * Find and delete a resource by its id
     *
     * @param int $id id to delete
     *
     * @return boolean
     */
    public function deleteById(int $id): bool
    {
        return $this->model
            ->findOrFail($id)
            ->delete();
    }

    /**
     * Sets up the new query builder object for this model
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function instantiateQuery()
    {
        return $this->model->query();
    }

    /**
     * Calls any scope methods on this model that are passed into the options
     *
     * @param \Illuminate\Database\Query\Builder $query Query builder object to extend
     * @param array $options Options to check
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected function callModelScopeMethods($query, $options = [])
    {
        foreach ($options as $key => $value) {
            if (method_exists($this->model, Str::camel('scope_'.$key))) {
                $query->{Str::camel($key)}($value);
            }
        }
        return $query;
    }

    /**
     * Attaches any associated models
     *
     * @param \Illuminate\Database\Query\Builder $query Query builder object to extend
     * @param array $options Options to check
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected function attachModels($query, $options = [])
    {
        if (isset($options['with']) && $options['with']) {
            if (is_array($options['with'])) {
                foreach ($options['with'] as $model) {
                    if (is_callable([$this->model, $model])) {
                        $this->defaultModels[] = $model;
                    }
                }
            } elseif (is_callable([$this->model, $options['with']])) {
                $this->defaultModels[] = $options['with'];
            }
        }
        return $query->with($this->defaultModels);
    }

    /**
     * Execute the paginated query
     *
     * @param \Illuminate\Database\Query\Builder $query Query builder object to extend
     * @param array $options Options to check
     *
     * @return LengthAwarePaginator
     */
    protected function executePaginatedQuery($query, $options = [])
    {
        return $query->orderBy(
            $this->getOrderBy($options),
            $this->getOrder($options)
        )->paginate(
            $this->getPerPage($options),
            ['*'],
            'page',
            $this->getPage($options)
        );
    }

    /**
     * Execute the query and get first result
     *
     * @param \Illuminate\Database\Query\Builder $query Query builder object to extend
     * @param array $options Options to check
     *
     * @return mixed
     */
    protected function executeQuery($query, $options = []): AbstractModel
    {
        return $query->firstOrFail();
    }

    /**
     * Get the order
     *
     * @param array $options Options to check
     *
     * @return string Order of results
     */
    protected function getOrder($options = [])
    {
        if (isset($options['order']) && in_array($options['order'], ['asc', 'desc'])) {
            return $options['order'];
        }
        return $this->defaultOrder;
    }

    /**
     * Get the field to order by
     *
     * @param array $options Options to check
     *
     * @return string Order of results
     */
    protected function getOrderBy($options = [])
    {
        if (isset($options['order_by'])) {
            foreach ($this->allowedOrderBy as $key => $field) {
                if ($options['order_by'] === $key) {
                    return $field;
                }
            }
        }
        return $this->defaultOrderBy;
    }

    /**
     * Get the number of results to return per page
     *
     * @param array $options Options to check
     *
     * @return string Results per page
     */
    protected function getPerPage($options = [])
    {
        return $options['per_page'] ?? env('PER_PAGE', 10);
    }

    /**
     * Get the number of results to return per page
     *
     * @param array $options Options to check
     *
     * @return string Results per page
     */
    protected function getPage($options = [])
    {
        return $options['page'] ?? null;
    }
}
