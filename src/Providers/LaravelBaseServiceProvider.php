<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Providers;

use Illuminate\Support\ServiceProvider;

class LaravelBaseServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../../config/permissions.php' => config_path('permissions.php'),
        ]);
    }
}
