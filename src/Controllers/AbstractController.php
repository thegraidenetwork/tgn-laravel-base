<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\HttpException;

abstract class AbstractController extends Controller
{
    use DispatchesJobs;

    /**
     * Validation rules for creating, updating, and deleting resources.
     *
     * @var array
     */
    public $rules = [];

    /**
     * Namespace to Jobs Classes (eg: '\GraideNetwork\Users\Jobs\GraiderReviews').
     *
     * @var string
     */
    protected $jobsPath;

    /**
     * Gets collection of objects from repository based on options from the query string.
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json(
            $this->resourceGet($this->options)
        );
    }

    /**
     * Gets a single objects from repository based on the id included in the route.
     *
     * @var $id integer Resource id
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function single(int $id = null): JsonResponse
    {
        return response()->json(
            $this->resourceGetById($id, $this->options)
        );
    }

    /**
     * Create new resource.
     *
     * @param Request $request
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $this->validate($this->data, 'create');

        $results = $this->resourceCreate($this->data);
        if ($results) {
            if (is_bool($results)) {
                return response()->json($results, 204);
            } else {
                return response()
                    ->json($results, 201)
                    ->header('Location', request()->path() . '/' . $results->id);
            }
        }
        return response()->json("Could not create.", 400);
    }

    /**
     * Update an entire existing resource.
     *
     * @param int $id
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function update(int $id = null): JsonResponse
    {
        $this->validate($this->data, 'create');

        if ($this->resourceUpdate($id, $this->data)) {
            return response()->json(null, 204);
        }

        return response()->json('Not found.', 404);
    }

    /**
     * Update specific fields in a resource.
     *
     * @param int $id
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function patch(int $id = null): JsonResponse
    {
        $this->validate($this->data, 'patch');

        if ($this->resourcePatch($id, $this->data)) {
            return response()->json(null, 204);
        }

        return response()->json('Not found.', 404);
    }

    /**
     * Delete a resource by id.
     *
     * @param int $id
     *
     * @throws \Exception
     * @return JsonResponse
     */
    public function delete(int $id = null): JsonResponse
    {
        $this->validate(['id' => $id], 'delete');

        if ($this->resourceDelete($id)) {
            return response()->json(null, 204);
        }

        return response()->json('Not found.', 404);
    }

    /**
     * Validate input data against the given rule type
     *
     * @param array $data
     * @param string $type
     *
     * @throws HttpException
     */
    protected function validate(array $data, string $type): void
    {
        if (!isset($this->rules[$type])) {
            throw new HttpException(500, "Controller '{$type}' rules not defined.");
        }

        Validator::validate($data, $this->rules[$type]);
    }

    /**
     * Get paginated collection of resources
     *
     * @param array $options
     *
     * @throws \Exception
     * @return LengthAwarePaginator
     */
    protected function resourceGet(array $options = []): LengthAwarePaginator
    {
        $className = $this->getJobClass('Get');
        return $this->dispatchNow(new $className($options));
    }

    /**
     * Get single resource by ID
     *
     * @param integer $id
     * @param array $options
     *
     * @throws \Exception
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function resourceGetById(int $id = null, array $options = []): Model
    {
        $className = $this->getJobClass('GetById');
        return $this->dispatchNow(new $className($id, $options));
    }

    /**
     * Create a single resource
     *
     * @param array $data
     *
     * @throws \Exception
     * @return mixed
     */
    protected function resourceCreate(array $data = [])
    {
        $className = $this->getJobClass('Create');
        return $this->dispatchNow(new $className($data));
    }

    /**
     * Update a single resource
     *
     * @param integer $id
     * @param array $data
     *
     * @throws \Exception
     * @return mixed
     */
    protected function resourceUpdate(int $id = null, array $data = [])
    {
        $className = $this->getJobClass('Update');
        return $this->dispatchNow(new $className($id, $data));
    }

    /**
     * Delete a single resource
     *
     * @param integer $id
     *
     * @throws \Exception
     * @return mixed
     */
    protected function resourceDelete(int $id = null)
    {
        $className = $this->getJobClass('Delete');
        return $this->dispatchNow(new $className($id));
    }

    /**
     * Patch (selective update) single resource with input data
     *
     * @param integer $id
     * @param array $data
     *
     * @throws \Exception
     * @return mixed
     */
    protected function resourcePatch(int $id = null, array $data = [])
    {
        return $this->resourceUpdate($id, $data);
    }

    /**
     * Returns the fully qualified job class name
     *
     * @param string $jobName
     *
     * @throws \Exception
     * @return string Path to job class
     */
    protected function getJobClass(string $jobName = null): string
    {
        if (!$this->jobsPath) {
            throw new \Exception('$jobsPath variable must be set on this class.');
        }
        return $this->jobsPath.'\\'.$jobName;
    }
}
