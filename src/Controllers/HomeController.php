<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * Displays a landing page for this application
     *
     * @return View
     */
    public function index(): View
    {
        return view('welcome');
    }
}
