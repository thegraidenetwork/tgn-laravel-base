<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Models;

use Illuminate\Database\Eloquent\Model;

abstract class AbstractModel extends Model
{
    /**
     * @var array Fields that can be filtered in this model
     */
    protected $allowedFilters = [];

    /**
     * @var array Fields that can be filtered by null in this model
     */
    protected $allowedNull = [];

    /**
     * @var array Fields that can be filtered by not null in this model
     */
    protected $allowedNotNull = [];

    /**
     * Filter results by array of fields and values
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param array $filters
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeFilters($query, $filters = [])
    {
        if ($filters && is_array($filters)) {
            foreach ($filters as $field => $value) {
                if (in_array($field, $this->allowedFilters)) {
                    $this->handleFilter($query, $field, $value);
                }
            }
        }
        return $query;
    }

    /**
     * Handles a single filter as either an array or key/value
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param string $field
     * @param $value
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected function handleFilter(&$query, string $field, $value = null)
    {
        return is_array($value) ?
            $query->whereIn($field, $value) :
            $query->where($field, $value);
    }

    /**
     * Get results where id is in array
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param array $ids
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeIds($query, $ids = [])
    {
        return $ids ? $query->whereIn($this->getTable().'.id', $ids) : $query;
    }

    /**
     * Get results where specific fields are null
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param array $fields Null fields
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeNullFields($query, $fields = [])
    {
        if ($fields && is_array($fields)) {
            foreach ($fields as $field) {
                if (in_array($field, $this->allowedNull)) {
                    $this->handleNullField($query, $field);
                }
            }
        }
        return $query;
    }

    /**
     * Handles the null field query for a single field
     *
     * @param Builder $query
     * @param string $field
     *
     * @return Builder
     */
    protected function handleNullField(&$query, string $field)
    {
        return $query->whereNull($field);
    }

    /**
     * Get results where specific fields are not null
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param array $fields Null fields
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeNotNullFields($query, $fields = [])
    {
        if ($fields && is_array($fields)) {
            foreach ($fields as $field) {
                if (in_array($field, $this->allowedNotNull)) {
                    $this->handleNotNullField($query, $field);
                }
            }
        }
        return $query;
    }

    /**
     * Handles the not null field query for a single field
     *
     * @param Builder $query
     * @param string $field
     *
     * @return Builder
     */
    protected function handleNotNullField(&$query, string $field)
    {
        return $query->whereNotNull($field);
    }
}
