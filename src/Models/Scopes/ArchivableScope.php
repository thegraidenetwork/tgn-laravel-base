<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Models\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ArchivableScope implements Scope
{
    protected $extensions = ['Archive', 'Recover', 'WithArchived', 'WithoutArchived', 'OnlyArchived'];

    public function apply(Builder $builder, Model $model): void
    {
        $builder->whereNull($model->getQualifiedArchivedAtColumn());
    }

    public function extend(Builder $builder): void
    {
        foreach ($this->extensions as $extension) {
            $this->{"add{$extension}"}($builder);
        }
    }

    protected function getArchivedAtColumn(Builder $builder): string
    {
        if (count((array) $builder->getQuery()->joins) > 0) {
            return $builder->getModel()->getQualifiedArchivedAtColumn();
        }

        return $builder->getModel()->getArchivedAtColumn();
    }

    protected function addArchive(Builder $builder): void
    {
        $builder->macro('archive', function (Builder $builder) {
            $column = $this->getArchivedAtColumn($builder);

            $builder->update([
                $column => $builder->getModel()->freshTimestampString(),
            ]);

            return $builder;
        });
    }

    protected function addRecover(Builder $builder): void
    {
        $builder->macro('recover', function (Builder $builder) {
            $builder->withArchived();
            $builder->update([$builder->getModel()->getArchivedAtColumn() => null]);

            return $builder;
        });
    }

    protected function addWithArchived(Builder $builder): void
    {
        $builder->macro('withArchived', function (Builder $builder, $withArchived = true) {
            if (!$withArchived) {
                return $builder->withoutArchived();
            }

            return $builder->withoutGlobalScope($this);
        });
    }

    protected function addWithoutArchived(Builder $builder): void
    {
        $builder->macro('withoutArchived', function (Builder $builder) {
            $model = $builder->getModel();

            $builder->withoutGlobalScope($this)->whereNull(
                $model->getQualifiedArchivedAtColumn()
            );

            return $builder;
        });
    }

    protected function addOnlyArchived(Builder $builder): void
    {
        $builder->macro('onlyArchived', function (Builder $builder) {
            $model = $builder->getModel();

            $builder->withoutGlobalScope($this)->whereNotNull(
                $model->getQualifiedArchivedAtColumn()
            );

            return $builder;
        });
    }
}
