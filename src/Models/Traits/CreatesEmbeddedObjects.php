<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Models\Traits;

use GraideNetwork\Base\Models\AbstractModel;
use Illuminate\Support\Collection;

trait CreatesEmbeddedObjects
{
    /**
     * @param string $embeddedObject
     * @param string $foreignKey
     * @param array $inputObjects
     * @param AbstractModel $model
     *
     * @return Collection
     */
    protected function createEmbeddedObjects(
        string $embeddedObject,
        string $foreignKey,
        array $inputObjects,
        AbstractModel $model
    ): Collection {
        $embeddedObjects = [];

        foreach ($inputObjects as $inputObject) {
            $embeddedObjects[] = $this->createEmbeddedObject(
                $embeddedObject,
                $foreignKey,
                $inputObject,
                $model
            );
        }

        return collect($embeddedObjects);
    }

    protected function createEmbeddedObject(
        string $embeddedObject,
        string $foreignKey,
        $inputObject,
        AbstractModel $model
    ): AbstractModel {
        $inputObject[$foreignKey] = $model->id;

        return $this->$embeddedObject()->make()->create($inputObject);
    }
}
