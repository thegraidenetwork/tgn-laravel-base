<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Models\Traits;

trait UpdatesEmbeddedObjects
{
    /**
     * Updates one or more embedded objects from a model
     *
     * @param string $embeddedObject
     * @param string $foreignKey
     * @param array $inputObjects
     *
     * @return void
     */
    protected function updateEmbeddedObjects(
        string $embeddedObject,
        string $foreignKey,
        $inputObjects = []
    ) {
        $updatedObjectIds = [];
        $objectsToCreate = [];

        foreach ($inputObjects as $inputObject) {
            // Update if exists, else save to create array
            if (isset($inputObject['id'])) {
                $this->$embeddedObject()->find($inputObject['id'])->update($inputObject);
                $updatedObjectIds[] = $inputObject['id'];
            } else {
                $objectsToCreate[] = $inputObject;
            }
        }

        // Delete any unused objects
        foreach ($this->$embeddedObject as $oldObject) {
            if (!in_array($oldObject->id, $updatedObjectIds)) {
                $oldObject->delete();
            }
        }

        // Create new objects
        foreach ($objectsToCreate as $objectToCreate) {
            $objectToCreate[$foreignKey] = $this->id;
            $this->$embeddedObject()->make()->create($objectToCreate);
        }
    }
}
