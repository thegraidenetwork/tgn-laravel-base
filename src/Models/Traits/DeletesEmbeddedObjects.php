<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Models\Traits;

trait DeletesEmbeddedObjects
{
    /**
     * Deletes each embedded object
     * @param string $embeddedObjectKey
     */
    protected function deleteEmbeddedObjects(string $embeddedObjectKey)
    {
        foreach ($this->$embeddedObjectKey as $obj) {
            $obj->delete();
        }
    }
}
