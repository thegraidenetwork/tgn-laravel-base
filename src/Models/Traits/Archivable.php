<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Models\Traits;

use GraideNetwork\Base\Models\Scopes\ArchivableScope;

trait Archivable
{
    public $archived = false;

    public static function bootArchivable(): void
    {
        static::addGlobalScope(new ArchivableScope);
    }

    public static function archiving($callback): void
    {
        static::registerModelEvent('archiving', $callback);
    }

    public static function archived($callback): void
    {
        static::registerModelEvent('archived', $callback);
    }

    public static function recovering($callback): void
    {
        static::registerModelEvent('recovering', $callback);
    }

    public static function recovered($callback): void
    {
        static::registerModelEvent('recovered', $callback);
    }

    public function __construct($attributes = array())
    {
        parent::__construct($attributes);

        $this->archived = $this->isArchived();
    }

    public function archive(): bool
    {
        if (is_null($this->getKeyName())) {
            throw new Exception('No primary key defined on model.');
        }

        if ($this->isArchived()) {
            return false;
        }

        if ($this->fireModelEvent('archiving') === false) {
            return false;
        }

        $this->touchOwners();
        $this->performArchiveOnModel();
        $this->fireModelEvent('archived', false);

        return true;
    }

    public function recover(): bool
    {
        if (!$this->isArchived()) {
            return false;
        }

        if ($this->fireModelEvent('recovering') === false) {
            return false;
        }

        $this->{$this->getArchivedAtColumn()} = null;
        $this->archived = false;

        $result = $this->save();
        $this->fireModelEvent('recovered', false);

        return $result;
    }

    public function isArchived(): bool
    {
        return !is_null($this->{$this->getArchivedAtColumn()});
    }

    public function getArchivedAtColumn(): string
    {
        return defined('static::ARCHIVED_AT') ? static::ARCHIVED_AT : 'archived_at';
    }

    public function getQualifiedArchivedAtColumn(): string
    {
        return $this->qualifyColumn($this->getArchivedAtColumn());
    }

    protected function performArchiveOnModel(): void
    {
        $query = $this->newQueryWithoutScopes()->where($this->getKeyName(), $this->getKey());
        $time = $this->freshTimestamp();
        $columns = [$this->getArchivedAtColumn() => $this->fromDateTime($time)];
        $this->{$this->getArchivedAtColumn()} = $time;

        if ($this->timestamps && ! is_null($this->getUpdatedAtColumn())) {
            $this->{$this->getUpdatedAtColumn()} = $time;
            $columns[$this->getUpdatedAtColumn()] = $this->fromDateTime($time);
        }

        $query->update($columns);
        $this->archived = true;
    }
}
