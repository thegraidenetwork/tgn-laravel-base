<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Jobs;

abstract class BaseUpdate
{
    /**
     * @var integer ID of resource to update
     */
    protected $id;

    /**
     * @var array Input data
     */
    protected $data;

    /**
     * Create a new job instance.
     *
     * @param int $id
     * @param array $data
     *
     * @return void
     */
    public function __construct(int $id, array $data = [])
    {
        $this->id = $id;
        $this->data = $data;
    }
}
