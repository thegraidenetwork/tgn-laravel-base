<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Jobs;

abstract class BaseCreate
{
    /**
     * @var array Input data
     */
    protected $data;

    /**
     * Create a new job instance.
     *
     * @param array $data
     *
     * @return void
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }
}
