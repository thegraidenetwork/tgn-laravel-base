<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Jobs;

abstract class BaseGetById
{
    /**
     * @var integer ID of resource to retrieve
     */
    protected $id;

    /**
     * @var array Input options
     */
    protected $options;

    /**
     * Create a new job instance.
     *
     * @param int $id
     * @param array $options
     *
     * @return void
     */
    public function __construct(int $id, array $options = [])
    {
        $this->id = $id;
        $this->options = $options;
    }
}
