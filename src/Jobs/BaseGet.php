<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Jobs;

abstract class BaseGet
{
    /**
     * @var array Input options
     */
    protected $options;

    /**
     * Create a new job instance.
     *
     * @param array $options
     *
     * @return void
     */
    public function __construct(array $options = [])
    {
        $this->options = $options;
    }
}
