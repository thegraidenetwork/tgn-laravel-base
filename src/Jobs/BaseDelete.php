<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Jobs;

abstract class BaseDelete
{
    /**
     * @var integer ID of message to delete
     */
    protected $id;

    /**
     * Create a new job instance.
     *
     * @param int $id
     *
     * @return void
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }
}
