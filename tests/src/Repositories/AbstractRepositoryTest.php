<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Tests\Repositories;

use GraideNetwork\Base\Tests\Fixtures\ConcreteModel;
use GraideNetwork\Base\Tests\TestCase;
use Illuminate\Pagination\LengthAwarePaginator;
use Mockery as m;
use GraideNetwork\Base\Tests\Fixtures\ConcreteRepository;

class AbstractRepositoryTest extends TestCase
{
    /**
     * @var ConcreteModel
     */
    protected $model;

    /**
     * @var ConcreteRepository
     */
    protected $repository;

    public function setUp(): void
    {
        parent::setUp();
        $this->model = m::mock(ConcreteModel::class);
        $this->repository = new ConcreteRepository($this->model);
    }

    public function testItCanGetPaginatedItems()
    {
        $items = m::mock(LengthAwarePaginator::class);

        $this->model->shouldReceive('query')
            ->once()
            ->andReturn($this->model);
        $this->model->shouldReceive('with')
            ->once()
            ->andReturn($this->model);
        $this->model->shouldReceive('orderBy')
            ->once()
            ->andReturn($this->model);
        $this->model->shouldReceive('paginate')
            ->with(env('PER_PAGE', 10), ['*'], 'page', null)
            ->once()
            ->andReturn($items);


        $results = $this->repository->get();

        $this->assertEquals($items, $results);
    }

    public function testItCanGetItemsWithOptions()
    {
        $items = m::mock(LengthAwarePaginator::class);

        $options = [
            'order_by' => 'col_1',
            'order' => 'desc',
            'per_page' => rand(3, 20),
            'page' => rand(1, 6),
            'with' => [
                'otherItems'
            ],
        ];

        $this->model->shouldReceive('query')
            ->once()
            ->andReturn($this->model);
        $this->model->shouldReceive('with')
            ->with([0 => 'otherItems'])
            ->once()
            ->andReturn($this->model);
        $this->model->shouldReceive('orderBy')
            ->with($options['order_by'], $options['order'])
            ->once()
            ->andReturn($this->model);
        $this->model->shouldReceive('paginate')
            ->with($options['per_page'], ['*'], 'page', $options['page'])
            ->once()
            ->andReturn($items);

        $results = $this->repository->get($options);

        $this->assertEquals($items, $results);
    }

    public function testItCanGetItemsBySpecificIds()
    {
        $items = m::mock(LengthAwarePaginator::class);

        $options = [
            'with' => [uniqid()],
            'ids' => [rand(), rand(), rand()]
        ];

        $this->model->shouldReceive('query')
            ->once()
            ->andReturn($this->model);
        $this->model->shouldReceive('ids')
            ->with($options['ids'])
            ->once()
            ->andReturn($this->model);
        $this->model->shouldReceive('with')
            ->with($options['with'])
            ->once()
            ->andReturn($this->model);
        $this->model->shouldReceive('orderBy')
            ->once()
            ->andReturn($this->model);
        $this->model->shouldReceive('paginate')
            ->with(env('PER_PAGE', 10), ['*'], 'page', null)
            ->once()
            ->andReturn($items);

        $results = $this->repository->get($options);

        $this->assertEquals($items, $results);
    }

    public function testItCanGetItemByAnyField()
    {
        $item = m::mock(ConcreteModel::class);
        $field = uniqid();
        $value = uniqid();

        $this->model->shouldReceive('where')
            ->with($field, $value)
            ->once()
            ->andReturn($this->model);
        $this->model->shouldReceive('with')
            ->with([])
            ->once()
            ->andReturn($this->model);
        $this->model->shouldReceive('firstOrFail')
            ->once()
            ->andReturn($item);

        $results = $this->repository->getBy($field, $value);

        $this->assertEquals($item, $results);
    }

    public function testItCanGetSingleItemById()
    {
        $item = m::mock(ConcreteModel::class);
        $id = rand(1, 50);
        $options = ['with' => 'otherItems'];

        $this->model->shouldReceive('where')
            ->with('id', $id)
            ->once()
            ->andReturn($this->model);
        $this->model->shouldReceive('with')
            ->with([0 => 'otherItems'])
            ->once()
            ->andReturn($this->model);
        $this->model->shouldReceive('firstOrFail')
            ->once()
            ->andReturn($item);

        $results = $this->repository->getById($id, $options);

        $this->assertEquals($item, $results);
    }

    public function testItThrowsExceptionWhenSingleItemNotFound()
    {
        $this->expectExceptionMessage('Resource Not Found');
        $this->expectException(\Exception::class);

        $item = m::mock(ConcreteModel::class);
        $id = rand(1, 50);
        $options = ['with' => 'otherItems'];

        $this->model->shouldReceive('where')
            ->with('id', $id)
            ->once()
            ->andReturn($this->model);
        $this->model->shouldReceive('with')
            ->with([0 => 'otherItems'])
            ->once()
            ->andReturn($this->model);
        $this->model->shouldReceive('firstOrFail')
            ->once()
            ->andThrow(new \Exception("Resource Not Found"));

        $results = $this->repository->getById($id, $options);

        $this->assertEquals($item, $results);
    }

    public function testItCanCreateItem()
    {
        $modelObject = m::mock(ConcreteModel::class);
        $data = ['name' => uniqid()];

        $this->model->shouldReceive('create')
            ->with($data)
            ->once()
            ->andReturn($modelObject);

        $results = $this->repository->create($data);

        $this->assertEquals($modelObject, $results);
    }

    public function testItCanUpdateAssignmentById()
    {
        $id = rand(1, 50);
        $data = ['name' => uniqid()];

        $this->model->shouldReceive('findOrFail')
            ->with($id)
            ->once()
            ->andReturn($this->model);
        $this->model->shouldReceive('update')
            ->with($data)
            ->once()
            ->andReturn(true);

        $results = $this->repository->updateById($id, $data);

        $this->assertTrue($results);
    }

    public function testItCanDeleteItemById()
    {
        $id = rand(1, 50);

        $this->model->shouldReceive('findOrFail')
            ->with($id)
            ->once()
            ->andReturn($this->model);
        $this->model->shouldReceive('delete')
            ->once()
            ->andReturn(true);

        $results = $this->repository->deleteById($id);

        $this->assertTrue($results);
    }
}
