<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Tests\Jobs;

use GraideNetwork\Base\Tests\Fixtures\ConcreteDeleteJob;
use GraideNetwork\Base\Tests\TestCase;

class BaseDeleteTest extends TestCase
{
    public function testItCanSetVariablesOnConstruct()
    {
        $id = rand();

        $job = new ConcreteDeleteJob($id);

        $this->assertEquals($id, $this->getProtectedProperty($job, 'id'));
    }
}
