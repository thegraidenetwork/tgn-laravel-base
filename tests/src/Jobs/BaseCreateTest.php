<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Tests\Jobs;

use GraideNetwork\Base\Tests\Fixtures\ConcreteCreateJob;
use GraideNetwork\Base\Tests\TestCase;

class BaseCreateTest extends TestCase
{
    public function testItCanSetVariablesOnConstruct()
    {
        $data = [
            uniqid() => uniqid(),
        ];
        $job = new ConcreteCreateJob($data);

        $this->assertEquals($data, $this->getProtectedProperty($job, 'data'));
    }
}
