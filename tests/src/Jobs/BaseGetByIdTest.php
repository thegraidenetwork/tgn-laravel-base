<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Tests\Jobs;

use GraideNetwork\Base\Tests\Fixtures\ConcreteGetByIdJob;
use GraideNetwork\Base\Tests\TestCase;

class BaseGetByIdTest extends TestCase
{
    public function testItCanSetVariablesOnConstruct()
    {
        $id = rand();
        $options = [
            uniqid() => uniqid(),
        ];
        $job = new ConcreteGetByIdJob($id, $options);

        $this->assertEquals($id, $this->getProtectedProperty($job, 'id'));
        $this->assertEquals($options, $this->getProtectedProperty($job, 'options'));
    }
}
