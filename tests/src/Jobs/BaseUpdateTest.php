<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Tests\Jobs;

use GraideNetwork\Base\Tests\Fixtures\ConcreteUpdateJob;
use GraideNetwork\Base\Tests\TestCase;

class BaseUpdateTest extends TestCase
{
    public function testItCanSetVariablesOnConstruct()
    {
        $id = rand();
        $data = [
            uniqid() => uniqid(),
        ];
        $job = new ConcreteUpdateJob($id, $data);

        $this->assertEquals($id, $this->getProtectedProperty($job, 'id'));
        $this->assertEquals($data, $this->getProtectedProperty($job, 'data'));
    }
}
