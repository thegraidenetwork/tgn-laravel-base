<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Tests\Jobs;

use GraideNetwork\Base\Tests\Fixtures\ConcreteGetJob;
use GraideNetwork\Base\Tests\TestCase;

class BaseGetTest extends TestCase
{
    public function testItCanSetVariablesOnConstruct()
    {
        $options = [
            uniqid() => uniqid(),
        ];
        $job = new ConcreteGetJob($options);

        $this->assertEquals($options, $this->getProtectedProperty($job, 'options'));
    }
}
