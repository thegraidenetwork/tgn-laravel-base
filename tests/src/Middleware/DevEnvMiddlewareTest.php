<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Tests\Middleware;

use GraideNetwork\Base\Tests\TestCase;
use Mockery as m;
use GraideNetwork\Base\Middleware\DevEnv;

class DevEnvMiddlewareTest extends TestCase
{
    public function testItAllowsRequestIfEnvNotProduction()
    {
        putenv('APP_ENV=development');
        $request = m::mock('\Illuminate\Http\Request');
        $middleware = new DevEnv;

        $result = $middleware->handle($request, function () {
            return 'can access';
        });

        $this->assertEquals('can access', $result);
    }

    public function testItDeniesRequestIfEnvIsProduction()
    {
        putenv('APP_ENV=production');
        $request = m::mock('\Illuminate\Http\Request');
        $middleware = new DevEnv;

        $result = $middleware->handle($request, function () {
            return 'can access';
        });

        $this->assertEquals('Illuminate\Http\Response', get_class($result));
    }
}
