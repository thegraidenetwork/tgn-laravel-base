<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Tests\Middleware;

use GraideNetwork\Base\Tests\TestCase;
use Mockery as m;
use GraideNetwork\Base\Middleware\Authenticate;

class AuthenticateMiddlewareTest extends TestCase
{
    public function setUp(): void
    {
        // Allows us to not skip the middleware since we're testing
        putenv('APP_ENV=development');
    }

    public function tearDown(): void
    {
        putenv('APP_ENV=testing');
    }

    public function testItDoesSkipsAuthWhenEnvIsTesting()
    {
        putenv('APP_ENV=testing');
        $request = m::mock('\Illuminate\Http\Request');
        $middleware = new Authenticate;

        $result = $middleware->handle($request, function () {
            return 'can access';
        });

        $this->assertEquals('can access', $result);
    }

    public function testItDoesNotAllowWhenPasswordInvalid()
    {
        $request = m::mock('\Illuminate\Http\Request');
        $middleware = new Authenticate;

        $request->shouldReceive('getUser')
            ->once()
            ->andReturn('admin');
        $request->shouldReceive('getPassword')
            ->once()
            ->andReturn(uniqid());

        $result = $middleware->handle($request, function () {
            return 'can access';
        });

        $this->assertEquals('Illuminate\Http\Response', get_class($result));
    }

    public function testItDoesAllowWhenCredentialsValid()
    {
        $request = m::mock('\Illuminate\Http\Request');
        $middleware = new Authenticate;

        $request->shouldReceive('getUser')
            ->once()
            ->andReturn(env('BASIC_AUTH_USERNAME'));
        $request->shouldReceive('getPassword')
            ->once()
            ->andReturn(env('BASIC_AUTH_PASSWORD'));

        $result = $middleware->handle($request, function () {
            return 'can access';
        });

        $this->assertEquals('can access', $result);
    }
}
