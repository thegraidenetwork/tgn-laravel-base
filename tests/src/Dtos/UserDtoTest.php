<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Tests\Dtos;

use GraideNetwork\Base\Dtos\UserDto;
use GraideNetwork\Base\Tests\TestCase;

class UserDtoTest extends TestCase
{
    public function setUp(): void
    {
        $this->validAttributes = [
            'ID' => uniqid(),
            'email' => uniqid(),
            'first_name' => uniqid(),
            'last_name' => uniqid(),
            'phone_number' => uniqid(),
        ];
        $this->invalidAttributes = [
            'train' => uniqid(),
            'marker' => uniqid(),
        ];
    }

    public function testItCanHydrateDto()
    {
        $this->attributes = array_merge($this->validAttributes, $this->invalidAttributes);
        $dto = UserDto::hydrate($this->attributes);
        $this->assertEquals($this->validAttributes, (array) $dto);
    }
}
