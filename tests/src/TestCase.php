<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Tests;

use Mockery as m;

class TestCase extends \PHPUnit\Framework\TestCase
{
    protected function tearDown(): void
    {
        m::close();
        parent::tearDown();
    }

    protected function getProtectedProperty($object, $property)
    {
        $class = new \ReflectionClass(get_class($object));

        $property = $class->getProperty($property);
        $property->setAccessible(true);

        return $property->getValue($object);
    }
}
