<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Tests\Models;

use GraideNetwork\Base\Tests\Fixtures\ConcreteModelWithTraits;
use GraideNetwork\Base\Tests\Fixtures\ConcreteModel;
use GraideNetwork\Base\Tests\TestCase;
use Illuminate\Support\Collection;
use Mockery as m;

class CreatesEmbeddedObjectsTest extends TestCase
{
    /**
     * @var ConcreteModelWithTraits
     */
    private $model;

    public function setUp(): void
    {
        parent::setUp();
        $this->model = m::mock(ConcreteModelWithTraits::class)->makePartial();
    }

    public function testItCanCreateArrayOfNewEmbeddedObjects()
    {
        $embeddedModelName = uniqid();
        $foreignKey = 'foreign_key';
        $inputObjects = [
            [
                'foreign_key' => uniqid(),
            ],
            [
                'foreign_key' => uniqid(),
            ],
        ];

        $embeddedModel = m::mock(ConcreteModel::class);
        $model = m::mock(ConcreteModel::class)->makePartial();
        $model->id = uniqid();
        $this->model->shouldReceive($embeddedModelName)
            ->times(count($inputObjects))
            ->andReturn($embeddedModel);

        $embeddedModel->shouldReceive('make')
            ->times(count($inputObjects))
            ->andReturnSelf();
        $embeddedModel->shouldReceive('create')
            ->times(count($inputObjects))
            ->andReturnSelf();

        $results = $this->model->createEmbeddedObjects($embeddedModelName, $foreignKey, $inputObjects, $model);

        $this->assertInstanceOf(Collection::class, $results);
        $this->assertEquals(count($inputObjects), count($results));
    }
}
