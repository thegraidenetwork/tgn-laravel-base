<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Tests\Models;

use GraideNetwork\Base\Tests\Fixtures\ConcreteModel;
use GraideNetwork\Base\Tests\Fixtures\ConcreteModelWithTraits;
use GraideNetwork\Base\Tests\TestCase;
use Mockery as m;

class UpdatesEmbeddedObjectsTest extends TestCase
{
    /**
     * @var ConcreteModelWithTraits
     */
    private $model;

    public function setUp(): void
    {
        parent::setUp();
        $this->model = m::mock(ConcreteModelWithTraits::class)->makePartial();
    }

    /**
     * @doesNotPerformAssertions
     */
    public function testItCanUpdateArrayOfExistingEmbeddedObjects()
    {
        $embeddedModelName = uniqid();
        $foreignKey = uniqid();
        $inputObjects = [
            [
                'id' => rand(),
                'field_1' => uniqid(),
            ],
            [
                'id' => rand(),
                'field_1' => uniqid(),
            ],
        ];

        $embeddedModel = m::mock(ConcreteModel::class);
        $this->model->shouldReceive($embeddedModelName)
            ->times(count($inputObjects))
            ->andReturn($embeddedModel);
        $this->model->$embeddedModelName = [
            (object) ['id' => $inputObjects[0]['id']],
            (object) ['id' => $inputObjects[1]['id']],
        ];

        $embeddedModel->shouldReceive('find')
            ->times(count($inputObjects))
            ->andReturnSelf();
        $embeddedModel->shouldReceive('update')
            ->times(count($inputObjects))
            ->andReturnSelf();

        $this->model->updateEmbeddedObjects($embeddedModelName, $foreignKey, $inputObjects);
    }

    /**
     * @doesNotPerformAssertions
     */
    public function testItCanCreateArrayOfNewEmbeddedObjects()
    {
        $embeddedModelName = uniqid();
        $foreignKey = uniqid();
        $inputObjects = [
            [
                'field_1' => uniqid(),
            ],
            [
                'field_1' => uniqid(),
            ],
        ];

        $embeddedModel = m::mock(ConcreteModel::class);
        $this->model->shouldReceive($embeddedModelName)
            ->times(count($inputObjects))
            ->andReturn($embeddedModel);
        $this->model->$embeddedModelName = [];

        $embeddedModel->shouldReceive('make')
            ->times(count($inputObjects))
            ->andReturnSelf();
        $embeddedModel->shouldReceive('create')
            ->times(count($inputObjects))
            ->andReturnSelf();

        $this->model->updateEmbeddedObjects($embeddedModelName, $foreignKey, $inputObjects);
    }

    /**
     * @doesNotPerformAssertions
     */
    public function testItCanDeleteArrayOfRemovedEmbeddedObjects()
    {
        $embeddedModelName = uniqid();
        $foreignKey = uniqid();
        $inputObjects = [];

        $embeddedModel = m::mock(ConcreteModel::class)->makePartial();
        $embeddedModel->id = uniqid();
        $this->model->$embeddedModelName = [
            $embeddedModel,
        ];

        $embeddedModel->shouldReceive('delete')
            ->once()
            ->andReturnSelf();

        $this->model->updateEmbeddedObjects($embeddedModelName, $foreignKey, $inputObjects);
    }
}
