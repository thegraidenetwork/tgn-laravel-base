<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Tests\Models;

use GraideNetwork\Base\Tests\Fixtures\ConcreteModel;
use GraideNetwork\Base\Tests\Fixtures\ConcreteModelWithTraits;
use GraideNetwork\Base\Tests\TestCase;
use Mockery as m;

class DeletesEmbeddedObjectsTest extends TestCase
{
    /**
     * @var ConcreteModelWithTraits
     */
    private $model;

    public function setUp(): void
    {
        parent::setUp();
        $this->model = m::mock(ConcreteModelWithTraits::class)->makePartial();
    }

    /**
     * @doesNotPerformAssertions
     */
    public function testItCanDeleteArrayOfExistingEmbeddedObjects()
    {
        $embeddedModelName = uniqid();

        $embeddedModel = m::mock(ConcreteModel::class)->makePartial();
        $embeddedModel->id = uniqid();
        $this->model->$embeddedModelName = [$embeddedModel];

        $embeddedModel->shouldReceive('delete')
            ->once()
            ->andReturnSelf();

        $this->model->deleteEmbeddedObjects($embeddedModelName);
    }
}
