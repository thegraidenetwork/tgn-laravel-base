<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Tests\Models;

use GraideNetwork\Base\Tests\TestCase;
use GraideNetwork\Base\Tests\Fixtures\ConcreteModel;

class AbstractModelTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->model = new ConcreteModel();
    }

    public function testItCanFilterFieldsByASingleValue()
    {
        $filters = [
            'filterable_field' => uniqid(),
        ];

        $this->model->builder->shouldReceive('from')
            ->andReturnSelf();
        $this->model->builder->shouldReceive('where')
            ->with('filterable_field', $filters['filterable_field'])
            ->once()
            ->andReturnSelf();

        $result = $this->model->filters($filters);

        $this->assertEquals('Illuminate\Database\Eloquent\Builder', get_class($result));
    }

    public function testItCanFilterFieldsByAnArrayOfValues()
    {
        $filters = [
            'filterable_field' => [uniqid(), uniqid(), uniqid()],
        ];

        $this->model->builder->shouldReceive('from')
            ->andReturnSelf();
        $this->model->builder->shouldReceive('whereIn')
            ->with('filterable_field', $filters['filterable_field'])
            ->once()
            ->andReturnSelf();

        $result = $this->model->filters($filters);

        $this->assertEquals('Illuminate\Database\Eloquent\Builder', get_class($result));
    }

    public function testItDoesNotFilterInvalidFields()
    {
        $filters = [
            'unfilterable_field' => uniqid(),
        ];
        $this->model->builder->shouldReceive('from')->andReturnSelf();

        $result = $this->model->filters($filters);

        $this->assertEquals('Illuminate\Database\Eloquent\Builder', get_class($result));
    }

    public function testItCanFilterByIds()
    {
        $ids = [
            uniqid(),
            uniqid(),
            uniqid(),
        ];

        $this->model->builder->shouldReceive('from')
            ->andReturnSelf();
        $this->model->builder->shouldReceive('whereIn')
            ->with('concrete_models.id', $ids)
            ->once()
            ->andReturnSelf();

        $result = $this->model->ids($ids);

        $this->assertEquals('Illuminate\Database\Eloquent\Builder', get_class($result));
    }

    public function testItCanFilterNullFields()
    {
        $fields = ['nullable_field'];

        $this->model->builder->shouldReceive('from')
            ->andReturnSelf();
        $this->model->builder->shouldReceive('whereNull')
            ->with($fields[0])
            ->once()
            ->andReturnSelf();

        $result = $this->model->nullFields($fields);

        $this->assertEquals('Illuminate\Database\Eloquent\Builder', get_class($result));
    }

    public function testItCanFilterMultipleNullFields()
    {
        $fields = ['nullable_field', 'another_nullable_field'];

        $this->model->builder->shouldReceive('from')
            ->andReturnSelf();
        $this->model->builder->shouldReceive('whereNull')
            ->with($fields[0])
            ->once()
            ->andReturnSelf();
        $this->model->builder->shouldReceive('whereNull')
            ->with($fields[1])
            ->once()
            ->andReturnSelf();

        $result = $this->model->nullFields($fields);

        $this->assertEquals('Illuminate\Database\Eloquent\Builder', get_class($result));
    }

    public function testItDoesNotFilterInvalidNullFields()
    {
        $fields = [
            'unnullable_field' => uniqid(),
        ];

        $this->model->builder->shouldReceive('from')
            ->andReturnSelf();

        $result = $this->model->nullFields($fields);

        $this->assertEquals('Illuminate\Database\Eloquent\Builder', get_class($result));
    }


    public function testItCanFilterNotNullFields()
    {
        $fields = ['non_nullable_field'];

        $this->model->builder->shouldReceive('from')
            ->andReturnSelf();
        $this->model->builder->shouldReceive('whereNotNull')
            ->with($fields[0])
            ->once()
            ->andReturnSelf();

        $result = $this->model->notNullFields($fields);

        $this->assertEquals('Illuminate\Database\Eloquent\Builder', get_class($result));
    }

    public function testItDoesNotFilterInvalidNotNullFields()
    {
        $fields = [
            'unnon_nullable_field' => uniqid(),
        ];

        $this->model->builder->shouldReceive('from')
            ->andReturnSelf();

        $result = $this->model->nullFields($fields);

        $this->assertEquals('Illuminate\Database\Eloquent\Builder', get_class($result));
    }
}
