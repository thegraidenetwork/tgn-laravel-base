<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Tests\Fixtures;

use Mockery as m;
use GraideNetwork\Base\Models\AbstractModel;

class ConcreteModel extends AbstractModel
{
    public $builder;

    protected $allowedFilters = ['filterable_field'];

    protected $allowedNull = ['nullable_field', 'another_nullable_field'];

    protected $allowedNotNull = ['non_nullable_field'];

    public function __construct(array $attributes = [])
    {
        // Mocking the query builder
        $this->builder = m::mock('Illuminate\Database\Query\Builder');

        // Mocking the DB connection
        $connection = m::mock('Illuminate\Database\Connection');
        static::$resolver = m::mock('Illuminate\Database\ConnectionResolverInterface');
        static::$resolver->shouldReceive('connection')
            ->andReturn($connection);

        parent::__construct($attributes);
    }

    protected function newBaseQueryBuilder()
    {
        return $this->builder;
    }
}
