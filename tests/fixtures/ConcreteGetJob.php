<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Tests\Fixtures;

use GraideNetwork\Base\Jobs\BaseGet;

class ConcreteGetJob extends BaseGet
{
}
