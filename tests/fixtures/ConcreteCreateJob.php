<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Tests\Fixtures;

use GraideNetwork\Base\Jobs\BaseCreate;

class ConcreteCreateJob extends BaseCreate
{
}
