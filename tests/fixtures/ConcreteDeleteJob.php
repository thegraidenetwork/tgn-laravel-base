<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Tests\Fixtures;

use GraideNetwork\Base\Jobs\BaseDelete;

class ConcreteDeleteJob extends BaseDelete
{
}
