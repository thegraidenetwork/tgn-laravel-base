<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Tests\Fixtures;

use GraideNetwork\Base\Repositories\AbstractRepository;
use GraideNetwork\Base\Repositories\Contracts\BaseRepositoryInterface;

class ConcreteRepository extends AbstractRepository implements BaseRepositoryInterface
{
    protected $allowedOrderBy = [
        'id' => 'id',
        'col_1' => 'col_1',
        'col_2' => 'col_2',
    ];

    public function __construct($model)
    {
        $this->model = $model;
    }
}
