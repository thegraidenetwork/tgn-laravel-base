<?php declare(strict_types = 1);

namespace GraideNetwork\Base\Tests\Fixtures;

use GraideNetwork\Base\Models\Traits\CreatesEmbeddedObjects;
use GraideNetwork\Base\Models\Traits\DeletesEmbeddedObjects;
use GraideNetwork\Base\Models\Traits\UpdatesEmbeddedObjects;

class ConcreteModelWithTraits
{
    use DeletesEmbeddedObjects, CreatesEmbeddedObjects, UpdatesEmbeddedObjects;
}
