<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Permissions Configuration
    |--------------------------------------------------------------------------
    |
    | Define the permissions for each path and method in your application here.
    |
    | Examples:
    |
    | [
    |     'path' => 'api/assignments',
    |     'method' => 'GET',
    |     'display_name' => 'Get assignments',
    |     'roles' => ['admin', 'ta', 't'],
    | ],
    | [
    |     // You can use regex for variables like this:
    |     'path' => 'api/assignments/\w+',
    |     'method' => 'GET',
    |     'display_name' => 'Get assignments',
    |     // You can use '*' to allow public access:
    |     'roles' => ['*'],
    | ],
    |
    */

];
