# Laravel base classes
### The Graide Network

This package contains commonly used classes for setting up new Laravel projects at The Graide Network.

## Requirements

- Laravel 6.0
- PHP 7.3+

## Installation

To get the latest stable version of this package add this line to your project's `composer.json` file:

```
 "thegraidenetwork/tgn-laravel-base": "^7.0"
```

You may also specify any version release listed on [Packagist](https://packagist.org/packages/thegraidenetwork/tgn-laravel-base). Check the [Changelog](changelog.md) for an overview of changes.

Next, add the `LaravelBaseServiceProvider` to your `config/app.php` file in the `providers` array:

```php
'providers' => [
    ...
    GraideNetwork\Base\Providers\LaravelBaseServiceProvider::class,
    ...
]
```

## Files

### Controllers
- **AbstractController** - The standard base controller for CRUD operations.
- **HomeController** - Displays a landing page for the API.

### Dtos
- **BaseDto** - Serves as a standard base data transfer object.
- **UserDto** - Allows other services to notify user objects that aren't complete models.

### Jobs
- **Base(_Action_)** - Abstract classes for dispatch jobs.

### Middleware
- **Authenticate** - Basic Auth gating for private projects.
- **DevEnv** - Prevents certain endpoints from being accessed in production.

### Models
- **AbstractModel** - Gives base model scopes for queries.

### Models/Scopes
- **ArchivableScope** - Archivable models are filtered by default. Additional `Builder` methods provided.

### Models/Traits
- **Archivable** - Models may be archived and recovered. Provides `archiving`, `archived`, `recovering`, and `recovered` events.
- **(_Creates/Deletes/Updates_)EmbeddedObjects** - Embedded objects can be updated within each model.

### Repositories
- **Contracts\BaseRepositoryInterface** - Serves as a standard base repository for most CRUD actions.
- **AbstractRepository** - Serves as a standard base repository for most CRUD actions.

## Updating this package
- Make changes.
- Add your updates to the [the changelog file](changelog.md).
- Update the readme.md file with any new information.
- Commit your changes.
- Create and push a tag: `git tag X.Y.Z` then `git push origin X.Y.Z` where `X.Y.Z` is the next version according to [Semver standards](http://semver.org/).
- Push your changes to the master branch: `git push origin master`.

## Tests
Note: This project uses Docker to run tests for local development. It is possible to run them locally on your machine's version of PHP, but this method is not documented.

- Install dependencies: `npm run -s composer:install` or update them: `npm run -s composer:update`
- Run the test suite: `npm run app:test`
- Run the linter: `npm run app:lint` or to fix automatically: `npm run app:lint:fix`.

Tests for the abstract classes in this package use concrete fixtures to instantiate new objects. A code coverage report is available in the `build/` directory after the first time tests are run.

## Releases
- See [the changelog](changelog.md). 
